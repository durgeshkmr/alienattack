var Spritesmith = require('spritesmith');
var fs = require('fs');
const sharp = require('sharp');



// generates sprites
const generateSprite = async()=>{

var imagesArr= [];
imagesArr.push('ship.png');
imagesArr.push('enemy_purple.png')
imagesArr.push('enemy_ship.png')
imagesArr.push('enemy_bee.png')
imagesArr.push('enemy_circle.png')
imagesArr.push('explosion.png')
imagesArr.push('missiles.png')
imagesArr.push('enemy_missiles.png')
//console.log(imagesArr)
Spritesmith.run({
    src: [
    'ship.png',
    'enemy_purple.png',
     'enemy_ship.png',
     'enemy_bee.png',
     'enemy_circle.png',
     'explosion.png',
     'missiles.png',
     'enemy_missiles.png'
  ],
  algorithm: 'left-right'
}, function handleResult (err, result) {
  // If there was an error, throw it
  if (err) {
    throw err;
  }
// console.log(result.coordinates['ship.png'].x);
  // Output the image

global.texturesMap = result.coordinates;
console.log('textureMap'+texturesMap['ship.png'].x)
  
  fs.writeFileSync(__dirname + '/1.png', result.image);
  result.coordinates, result.properties; // Coordinates and properties
  return texturesMap;
});


}


//fx. to resize image
const resizeImage = async(path)=>{
  sharp(path).resize({ height:32, width:32}).toFile(outputFilePath)
.then(function(newFileInfo){
console.log("Image Resized");
})
.catch(function(err){
console.log("Got Error");
});
}

//generateSprite();


 exports.generateSprite=generateSprite;
 exports.resizeImage=resizeImage